" Use Vim settings
" === .vimrc ===
"first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start
" Automatically change the current directory
set autochdir

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  set undofile		" keep an undo file (undo changes after closing)
endif

" Set Directories Swap Undo Backup
set directory=$HOME/.vim_temp/swap/
set backupdir=$HOME/.vim_temp/backup/
set undodir=$HOME/.vim_temp/undo/

" Command
set autowrite     " Automatically :write before running commands
set history=350		" keep 50 lines of command line history
set showcmd		" display incomplete commands


" -------
" Display
" -------
set laststatus=2    " Always display the status line
set ruler		" show the cursor position all the time
set colorcolumn=+1
set number " Lines
set textwidth=65 " Make it obvious where 70 characters is

" Colors
set t_Co=246
colorscheme inkpot



" Softtabs, 2 spaces
set tabstop=2
set shiftwidth=2
set shiftround
set expandtab


"" Remag gq for formating to line
"---------------------------------
"function! MyFormatExpr(start, end)
"    silent execute a:start.','.a:end.'s/[.!?]\zs /\r/g'
"endfunction

"Make '-' as part of identifier for autocompletion (CTRL-N/CTRL-P)?
set iskeyword+=\-


"Set dictionnaries
syntax on
set spell spelllang=en,de,fr,tr



"--------- FROM VIM CONFIG -------------
" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif


" Search
set incsearch		" do incremental searching
" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif
hi Search ctermfg=Red ctermbg=White
hi IncSearch ctermfg=White ctermbg=Yellow

" end of line whitespace are hilighted.
match Todo /\s\+$/




" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

" Header.R [ 2021-12-10 ]
autocmd bufnewfile * so $HOME/.vim_file_header
autocmd bufnewfile * exe "1," . 1 . "s/ current_dir/ " .expand("%:p:h:t")
autocmd bufnewfile * exe "1," . 1 . "s/0000-00-00/" .strftime("%Y-%m-%d")
autocmd bufnewfile * exe "1," . 2 . "s/ file_name / " .expand("%t ")
"autocmd Bufwritepre,filewritepre *.R exe ":silent!1," . 1 . "s/# \\zs\\w\\+\\ze |/" .expand("%:p:h:t")
"autocmd Bufwritepre,filewritepre *.R exe ":silent!1," . 1 . "v/".strftime("%Y-%m-%d")"/s/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]/" .strftime("%Y-%m-%d")
"autocmd Bufwritepre,filewritepre *.R exe ":silent!2," . 2 . "v/.*" .expand("%")"/s/=== \\S\\+/=== " .expand("%")"="
"autocmd Bufwritepre,filewritepre *.R exe "$" . "v/# bye-bye/s/$/\r\r# bye-bye....................|"
  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif



set formatexpr=MyFormatExpr(v:lnum,v:lnum+v:count-1)

" --------------------------------------------------
"                   #! NvimR !#
" --------------------------------------------------
let R_external_term = 0
let R_nvimpager = "vertical"
let R_pdfviewer = "mupdf"
let R_parenblock = 1
let R_assign_map = "$$"
autocmd FileType r setlocal formatoptions+=t "breaks lines auto



"R output is highlighted with current colorscheme
let rout_follow_colorscheme = 1
"R commands in R output are highlighted
let rout_more_colors = 1
" NvimR key bindings
vmap <Space> <Plug>RDSendSelection
nmap <Space> <Plug>RDSendLine
" Change Leader and LocalLeader keys:
   let maplocalleader = ','
   let mapleader = ','

" Use Ctrl+Space to do omnicompletion:
if has('nvim') || has('gui_running')
  inoremap <C-Space> <C-x><C-o>
else
   inoremap <Nul> <C-x><C-o>
endif

"Add the %>%
autocmd FileType r inoremap <buffer> <c-a> <Esc>:normal! a %>%<CR>a
autocmd FileType rmd inoremap <buffer> <c-a> <Esc>:normal! a %>%<CR>a
autocmd FileType r nnoremap <buffer> <c-y> <Esc>:call g:SendCmdToR("str(knitr::opts_chunk$get())")<CR>a
autocmd FileType rmd nnoremap  <c-y> <Esc>:call g:SendCmdToR("str(knitr::opts_chunk$get())")<CR>a

"Object Browser Options
let R_auto_start = 1 " R starts automatically even if Vim started
let R_objbr_opendf = 0 " Show non-exanped data.frames
let R_objbr_openlist = 0 " Show lists elements
let R_objbr_allnames = 0 " Show .GlobalEnv hidden objects
let R_objbr_labelerr = 1 " Warn if label is not a valid text

"function arguments displayed in Vim's status line when you insert `(`
let R_external_term = 'xterm -title R -e' 

" --------------------------------------------------
"                   #! Clip, Pathogen !#
" --------------------------------------------------
set clipboard=unnamedplus

syntax on
filetype plugin indent on
let g:tex_flavor = 'latex'

""Syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 0
"let g:syntastic_check_on_wq = 0
"let g:syntastic_enable_r_lintr_checker = 0
"let g:syntastic_r_checkers = ['lintr']

"Custom macros
" Delete trailing eol
nmap <F2> mx<esc>:%s/\s\+$//g<cr>`x<esc>:delmarks x<cr>zz
" insert today date
nmap <F3> :r! date +"\%F"<cr>
imap <F3> <C-g><C-t><C-r>=strftime("%F")<CR>
nmap <F4> :set spell spelllang=en_gb <cr>
nmap <F5> mx<esc>:set textwidth=72<cr>
nmap <F6> mx<esc>gqip`x<esc>:delmarks x<cr>zz
nmap <F7> mx<esc>gqG`x<esc>:delmarks x<cr>zz
nmap <F8> mx<esc>gqip`x<esc>:delmarks x<cr>zz

nmap <F9> ]s
nmap <F10> z=
" save
" ctrl + s
nmap <c-s> :w<CR>
vmap <c-s> <Esc><c-s>gv
imap <c-s> <Esc><c-s>

nmap <c-u> :update<CR>
vmap <c-u> <Esc><F2>gv
imap <c-u> <c-o><F2>

inoremap <C-W> <Esc>:execute "normal \<lt>C-W>\<lt>C-W>"<CR>
" Send text to next split
vnoremap <F5> y<c-w>wp<c-w>p

" Character & Symbols
imap <c-h> ḫ
imap <c-s-h> Ḫ
imap <c-b> &nbsp;
imap <c-9> „
imap <c-6> “
imap <c-l> ▣


"Remap hjkl ti jklö
noremap ö l
noremap l k
noremap k j
noremap j h
"
"Switch splitted windows
nnoremap <c-l> <c-w>ö
nnoremap <c-k> <c-w>l
nnoremap <c-j> <c-w>k
nnoremap <c-h> <c-w>j

" make in background
nnoremap <c-m> :execute 'silent make!' \| redraw!\|cc <CR>
" Natural splits
set splitbelow
set splitright

" Moving lines up or down
" -----------------------
"
" move the current line, or a selected block of lines
" map to kl not jk!
nnoremap <A-k> :m .+1<CR>==
nnoremap <A-l> :m .-2<CR>==
inoremap <A-k> <Esc>:m .+1<CR>==gi
inoremap <A-l> <Esc>:m .-2<CR>==gi
vnoremap <A-k> :m '>+1<CR>gv=gv
vnoremap <A-l> :m '<-2<CR>gv=gv


" Mapping Swapping! Woop Woop!
"
" source: https://vim.fandom.com/wiki/Swapping_characters,_words_and_lines#Mappings

" Note: Mappings above which perform a search-and-replace (ones
" containing :s//) will operate incorrectly on words with accented
" characters. To adjust the mappings above to work with your locale,
" replace all \w with [alphabet] and \W with [^alphabet], where alphabet
" is the set of characters in your alphabet. :help /\w
"
"For ISO/IEC_8859-1 Latin-1 Supplement characters, substitute all \w
"instances with [0-9A-Za-zÀ-ÖØ-öø-ÿ_] and all \_W instances with
"\_[^0-9A-Za-zÀ-ÖØ-öø-ÿ_].

" gc to swap the current character with the next, without changing the cursor position:
:nnoremap <silent> gc xph

" gw to swap the current word with the next, without changing cursor position:
" work across newlines:
:nnoremap <silent> gw "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>:nohlsearch<CR>

" gl to swap the current word with the previous
:nnoremap <silent> gl "_yiw?\w\+\_W\+\%#<CR>:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>:nohlsearch<CR>

" gr to swap the current word with the next
:nnoremap <silent> gr "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o>/\w\+\_W\+<CR><c-l>:nohlsearch<CR>

" g{ to swap the current paragraph with the next:
:nnoremap g{ {dap}p{

" Visual-mode swapping

" first, delete some text (using a command such as
" daw or dt in normal mode, or x in visual mode). Then, use visual mode
" to select some other text, and press Ctrl-X. The two pieces of text
" should then be swapped.

:vnoremap <C-X> <Esc>`.``gvP``P
:nnoremap <silent> gc xph


"" Make arrow keys useless...
"noremap  <Up> ""
"noremap! <a-k> <Down>
"noremap! <Up> <Esc>
"noremap  <Down> ""
"noremap! <Down> <Esc>
"noremap! <a-l> <Up>
"noremap  <Left> ""
"noremap! <a-j> <Left>
"noremap! <Left> <Esc>
"noremap  <Right> ""
"noremap! <Right> <Esc>
"noremap! <a-ö> <Right>

" Making kj ESC from insert
inoremap kj <Esc>

" vim-pandoc
let g:pandoc#modules#warn_disabled = 0
let g:pandoc#modules#disabled = ["folding"]
let g:pandoc#formatting#mode = ["A"]
let g:pandoc#formatting#textwidth = ["55"]

"" vim-sendtowindow
nnoremap ,r :.w !bash<CR>

function s:exec_on_term(lnum1, lnum2)
  " get terminal buffer
  let g:terminal_buffer = get(g:, 'terminal_buffer', -1)
  " open new terminal if it doesn't exist
  if g:terminal_buffer == -1 || !bufexists(g:terminal_buffer)
    terminal
    let g:terminal_buffer = bufnr('')
    wincmd p
  " split a new window if terminal buffer hidden
  elseif bufwinnr(g:terminal_buffer) == -1
    exec 'sbuffer ' . g:terminal_buffer
    wincmd p
  endif
  " join lines with "\<cr>", note the extra "\<cr>" for last line
  " send joined lines to terminal.
  call term_sendkeys(g:terminal_buffer,
        \ join(getline(a:lnum1, a:lnum2), "\<cr>") . "\<cr>")
endfunction

command! -range ExecOnTerm call s:exec_on_term(<line1>, <line2>)
nnoremap <leader>ex :ExecOnTerm<cr>
vnoremap <leader>ex :ExecOnTerm<cr>


" Boxes
" /*       _\|/_
"          (o o)
"  +----oOO-{_}-OOo-----------------------------------+
"  |" https://boxes.thomasjensen.com/docs/install.html|
"  +-------------------------------------------------*/
"
"
au BufRead,BufNewFile *.stc set filetype=".vim/ftdetect/boxes"

autocmd BufEnter * nmap ,mc !!boxes -d peek<CR>
autocmd BufEnter * vmap ,mc !boxes -d  peek<CR>
autocmd BufEnter * nmap ,xc !!boxes -d peek -r<CR>
autocmd BufEnter * vmap ,xc !boxes -d  peek -r<CR>

" Voom -----------------------
let voom_ft_modes = {'rmd': 'pandoc', 'rnoweb': 'latex'}
" ----------------------------------------------------------------
" fill rest of line with characters
" https://stackoverflow.com/a/3400528 [2021-12-10]
function! FillLine( str )
    " set tw to the desired total length
    " let tw = &textwidth
    " strip trailing spaces first
    .s/[[:space:]]*$//
    " calculate total number of 'str's to insert
    let reps = (30 - col("$")) / len(a:str)
    " insert them, if there's room, removing trailing spaces (though forcing
    " there to be one)
    if reps > 0
        .s/$/\=(' '.repeat(a:str, reps))/
    endif
endfunction

map <F12> :call FillLine( '-' )

" Insert current directory name
:inoremap ,wd <C-R>=getcwd()<CR>

"UltiSnips

nnoremap <leader>es :UltiSnipsEdit<cr>
" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
"" YouCompleteMe
let g:ycm_key_list_previous_completion=['<Up>']
"" Ultisnips
let g:UltiSnipsExpandTrigger="<c-tab>"
let g:UltiSnipsListSnippets="<c-s-tab>"
let g:UltiSnipsJumpForwardTrigger="<TAB-q>"
let g:UltiSnipsJumpBackwardTrigger="<c-w>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" From <https://github.com/honza/vim-snippets/blob/4600da87a064bf63b37c7135be154b34c40dec20/autoload/vim_snippets.vim>
" this is well known Filename found in snipmate (and the other engines), but
" rewritten and documented :)
"
" optional arg1: string in which to replace '$1' by filename with extension
"   and path dropped. Defaults to $1
" optional arg2: return this value if buffer has no filename
"  But why not use the template in this case, too?
"  Doesn't make sense to me
fun! Filename(...)
  let template = get(a:000, 0, "$1")
  let arg2 = get(a:000, 1, "")

  let basename = expand('%:t:r')

  if basename == ''
    return arg2
  else
    return substitute(template, '$1', basename, 'g')
  endif
endf

" original code:
" fun! Filename(...)
"     let filename = expand('%:t:r')
"     if filename == '' | return a:0 == 2 ? a:2 : '' | endif
"     return !a:0 || a:1 == '' ? filename : substitute(a:1, '$1', filename, 'g')
" endf

" yamllint
let g:syntastic_yaml_checkers = ['yamllint']
