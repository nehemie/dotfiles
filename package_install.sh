# # Install base package
sudo apt-get update \
  && apt-get install -y --no-install-recommends \
    make \
    xorg \
    i3 \
    i3status \
    i3lock \
    suckless-tools \
    xinit \
    wget \
    vim \
    xterm

sudo apt-get update \
  &&
  sudo apt-get install -y --no-install-recommends \
    gdal-bin \
    g++ \
    libgdal-dev \
    libproj-dev \
    libgeos-dev \
    libudunits2-dev \
    r-base \
    r-base-dev \
    r-recommended \
    r-cran-backports \
    r-cran-data.table \
    r-cran-dplyr \
    r-cran-ggplot2 \
    r-cran-gtools \
    r-cran-hmisc \
    r-cran-htmlwidgets \
    r-cran-jsonlite \
    r-cran-latticeextra \
    r-cran-maptools \
    r-cran-knitr \
    r-cran-plyr \
    r-cran-raster \
    r-cran-reshape \
    r-cran-rnaturalearthdata \
    r-cran-sf \
    r-cran-spatstat \
    r-cran-xtable \
    r-cran-zoo \
    && echo 'options(repos = c(CRAN = "https://cran.rstudio.com/"), download.file.method = "libcurl")' >> /etc/R/Rprofile.site

sudo apt-get update \
  && apt-get install -y --no-install-recommends \
    inkscape \
    texlive-base \
    texlive-fonts-recommended \
    texlive-xetex \
    biber \
    texlive-bibtex-extra \
    texlive-lang-french \
    texlive-lang-german \
    texlive-latex-extra \
    texlive-fonts-extra \
    texlive-science \
    fonts-linuxlibertine \
    pandoc \
    pandoc-citeproc \
    lmodern

#  Install git (to clone)
sudo apt-get update \
  && apt-get install -y --no-install-recommends git

# Divers software
sudo apt-get update \
  && apt-get install -y --no-install-recommends \
    mutt \
    muttprint \
    offlineimap \
    urlscan \
    w3m \
    cmus \
    vifm \
    ding trans-de-en\
    pass \
    cryptsetup \
    zsh \
    mupdf \
    pdftk \
    rsync \
    x11-xserver-utils \
    gimp \
    imagemagick \
    unison \
    libreoffice \
    feh \
    xclip \
    vim-gtk3 \
    ntfs-3g \
    gnuplot \
    ledger

# Since 2022
sudo apt-get update \
  && apt-get install -y --no-install-recommends \
  khal \
  vdirsyncer \
  ncal \
  pandoc-citeproc \
  r-cran-cowplot \
  && Rscript -e "install.packages(c('rnaturalearth', 'rnaturalearthdata'))"


sudo apt-get update \
  && sudo apt-get install -y --no-install-recommends \
    r-cran-biocmanager \
    r-bioc-complexheatmap \
    r-bioc-iranges \
    r-cran-httpuv \
  && sudo Rscript -e "install.packages(c('spiralize'))" \
  && sudo Rscript -e "install.packages(c('NutrienTrackeR'))"

sudo apt-get update \
  && sudo apt-get install -y --no-install-recommends \
  osmctools \
  firefox \
  flameshot

sudo apt-get update \
  && sudo apt-get install -y --no-install-recommends \
  vlc \
  transmission-cli \
  jabref \
  jmtpfs \
  jekyll

