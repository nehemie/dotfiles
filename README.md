# Dotfiles

These are "my" dotfiles, a patchwork from a lot of different
configuration files. 

It's Debian based, but I guess the software is more important than the
OS.

# License 

Well, I guess most of the content was copy-paste from Internet in one
way or another. It is online more for me (to be reusable on different
machines), but if you feel like using it, do it! 

Of course, feedback is always welcome.
