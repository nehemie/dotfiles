#!/usr/bin/env bash

# init: 2022-02-14
# directory: dotfiles/
# filename: dotfile_install.sh
# modification: 2022-02-14
#
# overview:
#   script to install non secret personal dotfiles
#

dotfiles_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

confirm_install() {
   read -p "Do you want to install $1 ? [Y/*]" -n 1 -r
   echo "" # (optional) move to a new line
   if [[ $REPLY =~ ^[Y]$ ]]
   then
     $2; $3
   else
     echo "Ok, no installed"
   fi
}

# Git
ln -sfv "${dotfiles_dir}/git/.gitconfig" "${HOME}"

# Bunch of symlinks for Shell
ln -sfv "${dotfiles_dir}/shell/.Xresources" "${HOME}"

# Bunch of symlinks for vim and plugins
vim_dot="${HOME}/.vim"
vim_temp="${HOME}/.vim_temp"
vim_pack_start="${HOME}/.vim/pack/start"
mkdir -p "${vim_dot}/spell" "${vim_dot}/colors"
mkdir -p "${vim_temp}/swap" "${vim_temp}/backup" "${vim_temp}/undo"
mkdir -p "${vim_pack_start}"

ln -sfv "${dotfiles_dir}/vim/vim_file_header" "${HOME}/.vim_file_header"
ln -sfv "${dotfiles_dir}/vim/.vimrc" "${HOME}"
ln -sfv "${dotfiles_dir}/vim/.gvimrc" "${HOME}"
ln -sfv "${dotfiles_dir}/vim/spell/de.utf-8.spl" "${vim_dot}/spell"
ln -sfv "${dotfiles_dir}/vim/spell/fr.utf-8.spl" "${vim_dot}/spell"
ln -sfv "${dotfiles_dir}/vim/spell/uk.utf-8.spl" "${vim_dot}/spell"
ln -sfv "${dotfiles_dir}/vim/spell/tr.utf-8.spl" "${vim_dot}/spell"
ln -sfv "${dotfiles_dir}/vim/colors/inkpot.vim"  "${vim_dot}/colors"

# from here: https://github.com/jalvesaq/Nvim-R
confirm_install 'Nvim-R from https://github.com/jalvesaq' \
   "git -C ${vim_pack_start} clone --depth=1 https://github.com/jalvesaq/Nvim-R"

# from here:
confirm_install 'vim-pandoc https://github.com/vim-pandoc/vim-pandoc' \
   "git -C ${vim_pack_start} clone --depth=1 https://github.com/vim-pandoc/vim-pandoc"

# from here:
confirm_install 'vim-pandoc https://github.com/vim-pandoc/vim-pandoc-syntax' \
  "git -C ${vim_pack_start} clone --depth=1 https://github.com/vim-pandoc/vim-pandoc-syntax"

# from here:
confirm_install 'syntastic https://github.com/vim-syntastic/syntastic' \
  "git -C ${vim_pack_start} clone --depth=1 https://github.com/vim-syntastic/syntastic.git"

# from here:
confirm_install 'ale https://github.com/w0rp/ale' \
  "git -C ${vim_pack_start} clone --depth=1 https://github.com/w0rp/ale"

# from here:
confirm_install 'vim-signature(marks) https://github.com/kshenoy/vim-signature' \
   "git -C ${vim_pack_start} clone --depth=1 https://github.com/kshenoy/vim-signature"

# from here:
confirm_install 'LaTeX-Box https://github.com/LaTeX-Box-Team/LaTeX-Box' \
   "git -C ${vim_pack_start} clone --depth=1 https://github.com/LaTeX-Box-Team/LaTeX-Box"

# /*       _\|/_
#          (o o)
#  +----oOO-{_}-OOo-+
#  |Ultisnips       |
#  +---------------*/
confirm_install 'UltiSnips https://github.com/sirver/ultisnips' \
   "git -C ${vim_pack_start} clone --depth=1 https://github.com/sirver/ultisnips"

mkdir -p  "${vim_dot}/UltiSnips/"

for file in "${dotfiles_dir}"/vim/Ultisnips/*; do
  ln -sfv "${file}" "${vim_dot}/UltiSnips/"
done


# i3
mkdir -p  "${HOME}/.config/i3/"
ln -sfv "${dotfiles_dir}/config/i3/config" "${HOME}/.config/i3/"

# inkscape
mkdir -p  "${HOME}/.config/inkscape/keys" "${HOME}/.config/inkscape/templates"
ln -sfv "${dotfiles_dir}/config/inkscape/preferences.xml" "${HOME}/.config/inkscape/"
ln -sfv "${dotfiles_dir}/config/inkscape/keys/default.xml" "${HOME}/.config/inkscape/keys"
for file in "${dotfiles_dir}"/config/inkscape/templates/*.svg; do
    ln -sfv "${file}" "${HOME}/.config/inkscape/templates/"
done

#xdg
ln -sfv "${dotfiles_dir}/config/user-dirs.dirs" "${HOME}/.config/"

# cmus
mkdir -p "${HOME}/.config/cmus/"
ln -sfv "${dotfiles_dir}/config/cmus/rc" "${HOME}/.config/cmus/"

# qpdfview
mkdir -p "${HOME}/.config/qpdfview/"
ln -sfv "${dotfiles_dir}/config/qpdfview/shortcuts.conf" "${HOME}/.config/qpdfview/"

# Bunch of symlinks for vifm
vifmcolor="${HOME}/.config/vifm/colors/Default.vifm"
mkdir -p  "${HOME}/.config/vifm/colors"
[ -f "${vifmcolor}" ] && mv "${vifmcolor}" "${vifmcolor}_backup" || echo "My file doesn't exist"
for file in "${dotfiles_dir}"/config/vifm/*; do
    ln -sfv "${file}" "${HOME}/.config/vifm/"
done
for file in "${dotfiles_dir}"/config/vifm/colors/*; do
    ln -sfv "${file}" "${HOME}/.config/vifm/colors/"
done

# Shell
# sh_aliases
ln -sfv "${dotfiles_dir}/shell/.profile" "${HOME}"
ln -sfv "${dotfiles_dir}/shell/.bashrc" "${HOME}"
ln -sfv "${dotfiles_dir}/shell/.sh_env" "${HOME}"
ln -sfv "${dotfiles_dir}/shell/.zshrc" "${HOME}"
ln -sfv "${dotfiles_dir}/shell/.zshrc.local" "${HOME}"
ln -sfv "${dotfiles_dir}/shell/.zshrc.zni" "${HOME}"

#Xterm
ln -sfv "${dotfiles_dir}/shell/.Xresources" "${HOME}"
ln -sfv "${dotfiles_dir}/shell/.xinitrc" "${HOME}"

#R
ln -sfv "${dotfiles_dir}/Rprofile" "${HOME}/.Rprofile"
ln -sfv "${dotfiles_dir}/RRetrowave.R" "${HOME}/.RRetrowave.R"

